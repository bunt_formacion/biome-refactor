( function() {
  'use strict';

  angular
      .module( 'app.routes.locations' )
      .run( appRun );

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun( routerHelper ) {
    routerHelper.configureStates( getStates() );
  }

  function getStates() {
    return [
            {
              state: 'locations',
              config: {
                parent: 'root.details',
                url: '/locations',
                views: {
                  'locations':{
                    templateUrl: 'app/routes/locations/locations.html',
                    controller: 'LocationsController',
                    controllerAs: 'vm'
                  }
                },
                sticky: true,
                deepStateRedirect: true,
                title: 'Locations',
                settings: {
                  nav: 3,
                  content: '<i class="locations-icon"></i> Locations'
                }
              }
            }
        ];
  }
} )();
