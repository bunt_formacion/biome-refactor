( function () {
  'use strict';

  angular
      .module( 'app.routes.locations' )
      .controller( 'LocationsController', LocationsController );

  LocationsController.$inject = ['$q', 'logger'];
  /* @ngInject */
  function LocationsController( $q, logger ) {
    var vm = this;

    vm.title = 'Locations';

  }
} )();
