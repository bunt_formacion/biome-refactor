/* jshint -W117, -W030 */
describe( 'LocationsController', function() {
  var controller;

  beforeEach( function() {
    bard.appModule( 'app.locations' );
    bard.inject( '$controller', '$log', '$q', '$rootScope' );
  } );

  beforeEach( function () {
    controller = $controller( 'LocationsController' );
    $rootScope.$apply();
  } );

  bard.verifyNoOutstandingHttpRequests();

  describe( 'Locations controller', function() {
    it( 'should be created successfully', function () {
      expect( controller ).to.be.defined;
    } );

    describe( 'after activate', function() {
      it( 'should have title of Locations', function () {
        expect( controller.title ).to.equal( 'Locations' );
      } );

      it( 'should have logged "Activated"', function() {
        expect( $log.info.logs ).to.match( /Activated/ );
      } );

    } );
  } );
} );
