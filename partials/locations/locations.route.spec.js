/* jshint -W117, -W030 */
describe( 'locations routes', function () {
  describe( 'state', function () {
    var controller;
    var view = 'app/locations/locations.html';

    beforeEach( function() {
      module( 'app.locations', bard.fakeToastr );
      bard.inject( '$httpBackend', '$location', '$rootScope', '$state', '$templateCache' );
    } );

    beforeEach( function() {
      $templateCache.put( view, '' );
    } );

    bard.verifyNoOutstandingHttpRequests();

    it( 'should map state locations to url /locations ', function() {
      expect( $state.href( 'locations', {} ) ).to.equal( '#/locations' );
    } );

    it( 'should map /locations route to locations View template', function () {
      expect( $state.get( 'locations' ).templateUrl ).to.equal( view );
    } );

    it( 'of locations should work with $state.go', function () {
      $state.go( 'locations' );
      $rootScope.$apply();
      expect( $state.is( 'locations' ) );
    } );
  } );
} );
