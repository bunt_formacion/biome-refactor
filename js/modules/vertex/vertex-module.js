(function () {
    'use strict';

    angular.module('vertex', [
        'vertex.cdf',
        'vertex.cde',
        'vertex.analyzer'
    ]);
})();




