(function () {
    'use strict';

    function CdfHelperProvider() {
        this.$get = ['$q', function ($q) {
            function getDashboardConstructor(path) {
                var deferred = $q.defer();

                require([path], deferred.resolve);

                // TODO: setTimeout or some other fail condition to reject promise??

                return deferred.promise;
            }

            function getNewDashboard(path, element) {
                return ( getDashboardConstructor(path).then(mountDashboard) );

                function mountDashboard(Dash) {
                    return ( new Dash(element) );
                }
            }

            function renderDashboard(dash) {
                var deferred = $q.defer();

                // TODO: Add observer, here.
                dash.listenToOnce(dash, 'cdf:postInit', resolvePromise);
                dash.render();

                function resolvePromise() {
                    deferred.resolve(dash);
                }

                return deferred.promise;
            }

            return {
                getNewDashboard: getNewDashboard,
                renderDashboard: renderDashboard
            };
        }];
    }

    angular
        .module('vertex.cdf')
        .provider('CdfHelper', [CdfHelperProvider]);
}());
