(function () {
    'use strict';
    function CdfDashboardController(CdfHelper) {
        var _dash;

        function setDashboard(dash) {
            _dash = dash;
            return dash;
        }

        function getDashboard() {
            return _dash;
        }

        function render() {
            return CdfHelper.renderDashboard(getDashboard());
        }

        function setNewDashboard(path, element) {
            return CdfHelper.getNewDashboard(path, element).then(setDashboard);
        }

        this.setDashboard = setDashboard;
        this.setNewDashboard = setNewDashboard;
        this.getDashboard = getDashboard;
        this.render = render;
    }

    function cdfDashboard($parse) {
        function postLink(scope, element, attrs, controller) {
            var _watcher;

            scope.$watch(function () {
                return controller.path
            }, handlePathChange);

            function handlePathChange(newPath, oldPath) {
                if (newPath) {
                    clearWatcher();
                    controller.setNewDashboard(newPath, element)
                        .then(setParameters)
                        .then(addEventsDispatcher)
                        .then(controller.render)
                        .then(addParametersWatcher);
                }
            }

            function addParametersWatcher(dash) {
                _watcher = scope.$watchCollection(function () {
                    return controller.getParameters();
                }, handleParametersChange, isDeepWatch());

                function handleParametersChange(newParameters, oldParameters) {
                    angular.forEach(newParameters, function (value, name) {
                        if (!angular.equals(value, dash.getParameterValue(name))) {
                            dash.fireChange(name, value);
                        }
                    });
                }

                return dash;
            }

            function addEventsDispatcher(dash) {
                dash.listenTo(dash, 'all', function (eventName, eventData) {
                    var callbacks = controller.getEvents() || {};
                    if (callbacks[eventName]) {
                        $parse(callbacks[eventName])(scope.$parent, {
                            name: eventName,
                            data: eventData
                        });
                    }
                });
                return dash;
            }

            function clearWatcher() {
                if (angular.isFunction(_watcher)) {
                    _watcher();
                    _watcher = undefined;
                }
            }

            function setParameters(dash) {
                angular.forEach(controller.getParameters(), function (value, name) {
                    dash.setParameter(name, value);
                });
                return dash;
            }

            function isDeepWatch() {
                return !!controller.isDeepWatch();
            }
        }

        return {
            bindToController: true,
            controller: ['CdfHelper', CdfDashboardController],
            controllerAs: 'vm',
            restrict: 'EA',
            scope: {
                'path': '@',
                'getParameters': '&parameters',
                'getEvents': '&events',
                'isDeepWatch': '&deepWatch'
            },
            link: ["scope", "element", "attrs", "controller", postLink]
        };
    }

    angular
        .module('vertex.cdf')
        .directive('cdfDashboard', ['$parse', cdfDashboard]);
}());
