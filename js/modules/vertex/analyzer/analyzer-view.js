(function () {
    'use strict';

    function AnalyzerView(AnalyzerHelper, uuid) {
        return {
            bindToController: true,
            controller: ['$scope', function ($scope) {
            }],
            controllerAs: 'vm',
            restrict: 'EA',
            template: '<iframe ng-src={{vm.path}} frameborder="0" id="{{vm.frameId}}" allowfullscreen/>',
            scope: {
                getConfig: '&config',
                onLoad: '&onLoad'
            },
            link: ["scope", "element", "attrs", "controller", function (scope, element, attrs, controller) {
                controller.frameId = '__ANALYZER__FRAME__' + uuid() + '__';

                scope.$watchCollection(function () {
                    return controller.getConfig();
                }, handleConfigChange);

                if (controller.onLoad) {
                    AnalyzerHelper.registerOnLoad(controller.frameId, onLoad);
                }

                function handleConfigChange(newConfig) {
                    controller.path = AnalyzerHelper.getAnalysisPath(newConfig);
                }

                function onLoad(api, frameId) {
                    controller.onLoad({api: api, frameId: frameId});
                }

            }]
        };
    }

    angular
        .module('vertex.analyzer')
        .directive('analyzerView', ['AnalyzerHelper', 'uuid', AnalyzerView]);
}());
