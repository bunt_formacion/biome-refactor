(function () {
    'use strict';

    function AnalyzerHelperProvider(UrlInterpolator) {
        var _basePath = '';

        function setBasePath(path) {
            _basePath = path;
        }

        function getBasePath() {
            return _basePath;
        }

        function getEndpointPath(endpoint) {
            return getBasePath() + '/' + endpoint;
        }

        function getEditorPath(path) {
            return getEndpointPath('editor');
        }

        function getAnalysisPath(config) {
            var url = ':basePath/:endpoint',
                params = {
                    basePath: getBasePath(),
                    endpoint: 'editor'
                };
            params = angular.extend(params, config);
            return UrlInterpolator(url, params).getUrl;
        }

        function AnalyzerHelper($window) {
            var onLoadHandlers = {};

            function registerOnLoad(frameId, callback) {
                if (angular.isString(frameId) && angular.isFunction(callback)) {
                    onLoadHandlers[frameId] = callback;
                }
            }

            function deregisterOnLoad(frameId) {
                delete onLoadHandlers[frameId];
            }

            function onAnalyzerLoad(api, frameId) {
                if (onLoadHandlers[frameId]) {
                    onLoadHandlers[frameId].apply(this, arguments);
                }
            }

            $window.onAnalyzerLoad = onAnalyzerLoad;

            return {
                getAnalysisPath: getAnalysisPath,
                registerOnLoad: registerOnLoad,
                deregisterOnLoad: deregisterOnLoad
            };
        }

        this.setBasePath = setBasePath;
        this.$get = ['$window', AnalyzerHelper];
    }

    angular
        .module('vertex.analyzer')
        .provider('AnalyzerHelper', ['UrlInterpolator', AnalyzerHelperProvider]);
}());
