(function () {
    'use strict';

    angular
        .module('vertex.analyzer', ['vertex.utils'])
        .config(['AnalyzerHelperProvider', function (AnalyzerHelperProvider) {
            AnalyzerHelperProvider.setBasePath('/pentaho/api/repos/xanalyzer');
        }]);
}());
