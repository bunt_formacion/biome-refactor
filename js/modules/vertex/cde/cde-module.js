(function () {
    'use strict';

    angular
        .module('vertex.cde', ['vertex.utils'])
        .config(['CdeHelperProvider', function (CdeHelperProvider) {
            CdeHelperProvider.setBasePath('/pentaho/plugin/pentaho-cdf-dd')
        }]);
}());
