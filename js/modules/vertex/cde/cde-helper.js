(function () {
    'use strict';

    function CdeHelperProvider(UrlInterpolator) {

        var _basePath = '';

        function setBasePath(path) {
            _basePath = path;
        }

        function getBasePath() {
            return _basePath;
        }

        function CdeHelper(UrlInterpolator) {

            function getDashboardPath(path) {
                var url = ':basePath/api/:endpoint';

                return UrlInterpolator(url, {
                    basePath: getBasePath(),
                    endpoint: 'renderer/getDashboard',
                    path: path
                }).getUrl;
            }

            return {
                getDashboardPath: getDashboardPath
            };
        }

        this.setBasePath = setBasePath;
        this.$get = CdeHelper;

    }

    angular.module('vertex.cde')
        .provider('CdeHelper', ['UrlInterpolator', CdeHelperProvider]);
}());
