(function () {
    'use strict';

    /**
     * Must configure the exception handling
     * @return {[type]}
     */
    function exceptionHandlerProvider() {
        /* jshint validthis:true */
        this.config = {
            appErrorPrefix: undefined
        };

        this.configure = function (appErrorPrefix) {
            this.config.appErrorPrefix = appErrorPrefix;
        };

        this.$get = function () {
            return {
                config: this.config
            };
        };
    }

    angular
        .module('blocks.exception')
        .provider('exceptionHandler', exceptionHandlerProvider);
}());
