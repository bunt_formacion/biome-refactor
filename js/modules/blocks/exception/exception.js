(function () {
    'use strict';

    function exception(logger) {
        function catcher(message) {
            return function (reason) {
                logger.error(message, reason);
            };
        }

        return {
            catcher: catcher
        };
    }

    angular
        .module('blocks.exception')
        .factory('exception', ['logger', exception]);
})();
