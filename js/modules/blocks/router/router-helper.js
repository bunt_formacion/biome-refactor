(function () {
    'use strict';

    function routerHelperProvider($locationProvider, $stateProvider, $urlRouterProvider) {
        /* jshint validthis:true */
        var config = {
            docTitle: undefined,
            resolveAlways: {}
        };

        function RouterHelper($location, $rootScope, $state, logger) {
            var handlingStateChangeError = false,
                hasOtherwise = false,
                stateCounts = {
                    errors: 0,
                    changes: 0
                };

            function configureStates(states, otherwisePath) {
                states.forEach(function (state) {
                    state.config.resolve =
                        angular.extend(state.config.resolve || {}, config.resolveAlways);
                    $stateProvider.state(state.state, state.config);
                });
                if (otherwisePath && !hasOtherwise) {
                    hasOtherwise = true;
                    $urlRouterProvider.otherwise(otherwisePath);
                }
            }

            function handleRoutingErrors() {
                // Route cancellation:
                // On routing error, go to the dashboard.
                // Provide an exit clause if it tries to do it twice.
                $rootScope.$on('$stateChangeError',
                    function (event, toState, toParams, fromState, fromParams, error) {
                        if (handlingStateChangeError) {
                            return;
                        }
                        stateCounts.errors++;
                        handlingStateChangeError = true;
                        var destination = ( toState &&
                            ( toState.title || toState.name || toState.loadedTemplateUrl ) ) ||
                            'unknown target';
                        var msg = 'Error routing to ' + destination + '. ' +
                            ( error.data || '' ) + '. <br/>' + ( error.statusText || '' ) +
                            ': ' + ( error.status || '' );
                        logger.warning(msg, [toState]);
                        $location.path('/');
                    }
                );
            }

            function isCurrentAncestor(ancestorName) {
                var state = $state.$current;
                while (state) {
                    if (state.name == ancestorName) {
                        return true;
                    }
                    state = state.parent;
                }
                return undefined;
            }

            function getStates() {
                return $state.get();
            }

            function getChildren(parentName) {
                return getStates().filter(function (r) {
                    return ( r.parent == parentName );
                });
            }

            function updateDocTitle() {
                $rootScope.$on('$stateChangeSuccess',
                    function (event, toState, toParams, fromState, fromParams) {
                        stateCounts.changes++;
                        handlingStateChangeError = false;
                        var title = config.docTitle + ' ' + ( toState.title || '' );
                        $rootScope.title = title; // data bind to <title>
                    }
                );
            }

            handleRoutingErrors();
            updateDocTitle();

            return {
                configureStates: configureStates,
                getStates: getStates,
                getChildren: getChildren,
                stateCounts: stateCounts,
                isCurrentAncestor: isCurrentAncestor
            };
        }

        $locationProvider.html5Mode(false);

        this.configure = function (cfg) {
            angular.extend(config, cfg);
        };

        this.$get = ['$location', '$rootScope', '$state', 'logger', RouterHelper];
    }

    angular
        .module('blocks.router')
        .provider('routerHelper', ['$locationProvider', '$stateProvider', '$urlRouterProvider', routerHelperProvider]);
})();
