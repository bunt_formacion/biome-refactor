(function () {
    'use strict';

    angular
        .module('app.core', [
            'ngAnimate',
            'ngSanitize',
            'blocks.exception',
            'blocks.logger',
            'blocks.router',
            'vertex',
            'ui.router',
            'ct.ui.router.extras.core',
            'ct.ui.router.extras.sticky',
            'ct.ui.router.extras.dsr',
            'ngMaterial',
            '720kb.tooltips'
        ])
        .constant('moment', moment)
        .constant('config', {
            appErrorPrefix: '[BioMe Error] ',
            appTitle: 'BioMe'
        })
        .config(['$logProvider', 'routerHelperProvider', 'exceptionHandlerProvider', 'config',
            function ($logProvider, routerHelperProvider, exceptionHandlerProvider, config) {
                if ($logProvider.debugEnabled) {
                    $logProvider.debugEnabled(true);
                }
                exceptionHandlerProvider.configure(config.appErrorPrefix);
                routerHelperProvider.configure({docTitle: config.appTitle + ': '});
            }])
        .config(['$mdThemingProvider', function ($mdThemingProvider) {
            $mdThemingProvider.theme('biome');
            $mdThemingProvider.setDefaultTheme('biome');
        }])
        .run(['routeHelper', function (routerHelper) {
            var otherwise = '/';
            routerHelper.configureStates([{
                state: '404',
                config: {
                    url: '/404',
                    templateUrl: 'app/core/404.html',
                    title: '404'
                }
            }], otherwise);
        }]);

})();
