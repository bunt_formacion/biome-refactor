(function () {
    'use strict';

    angular.module('app.routes', [
        'app.core',
        'app.routes.analytics',
        'app.routes.financialImpact',
        'app.routes.locations',
        'app.routes.recentStudies'
    ]);
})();
