(function () {
    'use strict';

    function FinancialImpactController($q, logger) {
        var vm = this;

        vm.title = 'FinancialImpact';
    }

    angular
        .module('app.routes.financialImpact')
        .run(['routerHelper', function (routerHelper) {
            routerHelper.configureStates({
                state: 'financialImpact',
                config: {
                    parent: 'root.details',
                    url: '/financial',
                    views: {
                        "financialImpact": {
                            templateUrl: 'partials/financial-impact/financial-impact.html',
                            controller: ['$q', 'logger', FinancialImpactController],
                            controllerAs: 'vm'
                        }
                    },
                    sticky: true,
                    deepStateRedirect: true,
                    title: 'Financial Impact',
                    settings: {
                        nav: 2,
                        content: '<i class="financial-impact-icon"></i> <span>Financial Impact</span>'
                    }
                }
            });
        }])
}());
