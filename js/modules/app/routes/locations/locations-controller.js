(function () {
    'use strict';

    function LocationsController($q, logger) {
        var vm = this;

        vm.title = 'Locations';

    }

    angular
        .module('app.routes.locations')
        .run(['routerHelper', function (routerHelper) {
            routerHelper.configureStates({
                state: 'locations',
                config: {
                    parent: 'root.details',
                    url: '/locations',
                    views: {
                        'locations': {
                            templateUrl: 'partials/locations/locations.html',
                            controller: ['$q', 'logger', LocationsController],
                            controllerAs: 'vm'
                        }
                    },
                    sticky: true,
                    deepStateRedirect: true,
                    title: 'Locations',
                    settings: {
                        nav: 3,
                        content: '<i class="locations-icon"></i> <span>Locations</span>'
                    }
                }
            });
        }]);
}());
