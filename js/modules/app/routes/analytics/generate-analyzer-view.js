(function () {
    'use strict';

    function generateAnalyzerView($scope, configGenerator, filterFn) {
        return function (api, frameId) {
            api.event.registerInitListener(function (e, cv) {
                $scope.$watch(filterFn, updateFilter);
                updateFilter($scope.$eval(filterFn));

                function updateFilter(newValue, oldValue) {
                    if (newValue !== oldValue) {
                        $scope.$evalAsync(function () {
                            updateReport(configGenerator(newValue), cv);
                        });
                    }
                }

            });

            function updateReport(settings, cv) { // ,report
                // Visualization
                cv.api.report.setVizId(settings.vizId);
                angular.forEach(settings.chartOptions, function (value, key) {
                    cv.api.report.setChartOption(key, value);
                });

                // Data Layout
                angular.forEach(settings.layout, function (value, key) {
                    cv.api.report.setLayoutFields(key, value);
                });

                angular.forEach(settings.filters, function (value, key) {
                    cv.api.report.setFilters(key, value);
                });
                angular.forEach(settings.fieldOptions, function (opts, field) {
                    angular.forEach(opts, function (value, key) {
                        cv.api.report.setFieldOption(field, key, value);
                    });
                });

                // UI
                // cv.api.ui.removeMainToolbar(false);
                // cv.api.ui.removeHeaderBar(true);
                // cv.api.ui.removeUndoButton( true );
                // cv.api.ui.removeRedoButton( true );

                // cv.api.ui.showFieldLayout( false );
                // cv.api.ui.showFieldList( false );
                // cv.api.ui.showFilterPanel( false );

                cv.api.operation.refreshReport();
            }

        };

    }

    angular
        .module('app.routes.analytics')
        .constant('generateAnalyzerView', generateAnalyzerView);
}());
