(function () {
    'use strict';

    function HeartRateVsSugarController($scope, $stateParams, generateAnalyzerView, analyzerData) {
        var vm = this,
            stateName = 'analytics.analyze.heart-rate-vs-sugar';

        vm.analyzerConfig = {
            cube: $stateParams.request,
            catalog: $stateParams.request,
            embeddingContext: 'biome,heart-rate-vs-systolic-pressure',
            removeUndoButton: true,
            removeRedoButton: true,
            showFieldLayout: false,
            showFieldList: false,
            showFilterPanel: false
        };

        vm.onLoad = generateAnalyzerView($scope, getViewConfig, function () {
            return analyzerData.filterValue;
        });

        function getViewConfig(filterValue) {
            return {
                layout: {
                    x: ["[Measures].[Avg Systolic Blood Pressure]"],
                    y: ["[Measures].[Avg Heart Rate BPM]"],
                    rows: ["[Health Stat].[Avg Sugar Level MG]"]
                },
                filters: {
                    "[Date.Year].[Year]": (function (filterValue) {
                        return [{
                            "operator": "CONTAIN",
                            "members": [filterValue + ""]
                        }];
                    })(filterValue)
                },
                // filters: {
                //   "[Date.Year].[Year]": [{
                //     "operator": "CONTAIN",
                //     "members": [filterValue + ""]
                //   }]
                // },
                vizId: "ccc_scatter",
                title: "Heart Rate vs. Sugar Level",
                chartOptions: {
                    labelStyle: "BOLD"
                }
            };
        }

    }

    function HeartRateVsSugarSelectorController(analyzerData) {
        var vm = this,
            stateName = 'analytics.analyze.heart-rate-vs-sugar';

        vm.data = analyzerData;

    }

    angular
        .module('app.routes.analytics')
        .run(['routerHelper', function (routerHelper) {
            routerHelper.configureStates({
                state: 'analytics.analyze.heart-rate-vs-sugar',
                config: {
                    parent: 'analytics.analyze',
                    url: '/heart-rate-vs-systolic-pressure',
                    deepStateRedirect: true,
                    sticky: true,
                    resolve: {
                        analyzerData: function () {
                            return {
                                filterValue: 2010
                            };
                        }
                    },
                    views: {
                        'analytics.analyze.heart-rate-vs-sugar': {
                            templateUrl: 'partials/analytics/analyze/heart-rate-vs-sugar/heart-rate-vs-sugar-content.html',
                            controller: ['$scope', '$stateParams', 'generateAnalyzerView', 'analyzerData', HeartRateVsSugarController],
                            controllerAs: 'vm'
                        },
                        'analysis-title': {
                            template: 'Heart Rate vs. Systolic Blood Pressure'
                        },
                        'analysis-selector': {
                            templateUrl: 'partials/analytics/analyze/heart-rate-vs-sugar/heart-rate-vs-sugar-selector.html',
                            controller: ['analyzerData', HeartRateVsSugarSelectorController],
                            controllerAs: 'vm'
                        }
                    },
                    settings: {
                        nav: 3,
                        content: '<div class="toggle-button-icon scatterplot-icon"></div>'
                    },
                    title: 'Heart Rate vs. Systolic Blood Pressure'
                }
            });
        }]);
}());
