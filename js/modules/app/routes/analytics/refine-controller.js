(function () {
    'use strict';

    function RefineController($scope, $state, $q, $mdDialog, $timeout) {
        var vm = this,
            _isOpen = false,
            _deferred;

        vm.title = 'Refine';
        vm.dashboards = _dashboards;
        vm.param = {};

        vm.toggle = toggle;
        vm.isOpen = isOpen;
        vm.updateParameter = _.throttle(updateParameter, 500);
        vm.showDialog = showDialog;
        vm.resolveRequest = resolveRequest;
        vm.goToAnalyzer = goToAnalyzer;
        vm.toggleProgressIndicator = toggleProgressIndicator;
        vm.hasProgressIndicator = hasProgressIndicator;

        function toggleProgressIndicator(value, dashboard) {
            vm.dashboards[dashboard].showProgress = !!value;
        }

        function hasProgressIndicator(dashboard) {
            return !!vm.dashboards[dashboard].showProgress;
        }

        function isOpen() {
            return _isOpen || undefined;
        }

        function toggle() {
            _isOpen = !_isOpen;
        }

        function updateParameter(name, data) {
            $timeout(function () {
                vm.param = data;
            }, 0);
        }

        function goToAnalyzer(data) {
            $state.go(
                'analytics.analyze',
                {request: data.catalog || data.cube}
            );
        }

        function startRequest() {
            _deferred = $q.defer();
        }

        function resolveRequest() {
            if (_deferred) {
                _deferred.resolve();
            }
        }

        function getRequest() {
            return _deferred;
        }

        function showDialog(eventData) {
            var parentEl = angular.element(document.body);

            startRequest();

            $mdDialog.show({
                parent: parentEl,
                templateUrl: 'partials/analytics/refine/analyze-dialog.html',
                locals: {
                    action: action,
                    actionData: eventData,
                    deferredResponse: getRequest()
                },
                controller: 'DialogController'
            });

            toggle();

            function action() {
                goToAnalyzer(eventData);
            }

        }

    }

    angular
        .module('app.routes.analytics')
        .run(['routerHelper', function (routerHelper) {
            routerHelper.configureStates({
                state: 'analytics.refine',
                config: {
                    parent: 'analytics',
                    url: '/refine',
                    deepStateRedirect: true,
                    sticky: true,
                    views: {
                        'analytics.refine': {
                            templateUrl: 'partials/analytics/refine/refine.html',
                            controller: ['$scope', '$state', '$q', '$mdDialog', '$timeout', RefineController],
                            controllerAs: 'vm'
                        }
                    },
                    settings: {
                        nav: 1
                    },
                    title: 'Refine'
                }
            });
        }])
}());
