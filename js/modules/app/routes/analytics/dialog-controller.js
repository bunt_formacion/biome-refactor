(function () {
    'use strict';

    function DialogController($scope, $mdDialog, $mdToast, action, deferredResponse, actionData) {
        var _isReady = false;

        $scope.hide = hide;
        $scope.runAndHide = runAndHide;
        $scope.isReady = isReady;
        $scope.hideAndNotify = hideAndNotify;
        $scope.name = actionData.catalog || actionData.cube;

        deferredResponse.promise.then(function () {
            _isReady = true;
        });

        function runAndHide() {
            // The action order is reversed from the function name but... come on... it HAD to be this way!
            hide();
            action();
        }

        function hideAndNotify() {
            hide();
            notify();
        }

        function hide() {
            $mdDialog.hide();
        }

        function isReady() {
            return _isReady;
        }

        function notify() {
            deferredResponse.promise.then(function () {
                $mdToast.show({
                    position: 'top right',
                    controller: 'ToastController',
                    hideDelay: 0,
                    locals: {
                        action: action,
                        actionData: actionData
                    },
                    templateUrl: 'partials/analytics/refine/analyze-notification.html'
                });
            });
        }

    }

    angular
        .module('app.routes.analytics')
        .controller('DialogController', ['$scope', '$mdDialog', '$mdToast', 'action', 'deferredResponse', 'actionData', DialogController]);
}());
