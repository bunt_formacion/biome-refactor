(function () {
    'use strict';

    function AnalyticsController($scope, $state, routerHelper) {
        var vm = this,
            stateName = 'analytics';

        function goToChild(currentState) {
            if (currentState == stateName && vm.navRoutes[0]) {
                $state.go(vm.navRoutes[0].name);
            }
        }

        function getNavRoutes() {
            return routerHelper.getChildren(stateName)
                .sort(function (r1, r2) {
                    return r1.settings.nav - r2.settings.nav;
                });
        }

        vm.title = 'Analytics';
        vm.isCurrent = routerHelper.isCurrentAncestor;
        vm.navRoutes = getNavRoutes();

        $scope.$watch(function () {
            return $state.$current.name;
        }, goToChild);
    }

    angular
        .module('app.routes.analytics')
        .run(['routerHelper', function (routerHelper) {
            routerHelper.configureStates({
                state: 'analytics',
                config: {
                    parent: 'root.details',
                    url: '/analytics',
                    views: {
                        "analytics": {
                            templateUrl: 'partials/analytics/analytics.html',
                            controller: ['$scope', '$state', 'routerHelper', AnalyticsController],
                            controllerAs: 'vm'
                        }
                    },
                    sticky: true,
                    deepStateRedirect: true,
                    title: 'Analytics',
                    settings: {
                        nav: 1,
                        content: '<i class="analytics-icon"></i> <span>Analytics</span>'
                    }
                }
            });
        }])
}());
