(function () {
    'use strict';

    function AnalyzeController($scope, $state, $stateParams, routerHelper) {
        var vm = this,
            stateName = 'analytics.analyze';

        function getNavRoutes() {
            return routerHelper.getChildren(stateName)
                .sort(function (r1, r2) {
                    return r1.settings.nav - r2.settings.nav;
                });
        }

        function goToChild(currentState) {
            if (currentState == stateName && vm.navRoutes[0]) {
                $state.go(vm.navRoutes[0].name);
            }
        }

        vm.isCurrent = routerHelper.isCurrentAncestor;
        vm.navRoutes = getNavRoutes();
        vm.requestName = $stateParams.request;

        $scope.$watch(function () {
            return $state.$current.name;
        }, goToChild);
    }

    angular
        .module('app.routes.analytics')
        .run(['routerHelper', function (routerHelper) {
            routerHelper.configureStates({
                state: 'analytics.analyze',
                config: {
                    parent: 'analytics',
                    url: '/analyze/:request',
                    sticky: true,
                    views: {
                        'analytics.analyze': {
                            templateUrl: 'partials/analytics/analyze/analyze.html',
                            controller: ['$scope', '$state', '$stateParams', 'routerHelper', AnalyzeController],
                            controllerAs: 'vm'
                        }
                    },
                    settings: {
                        nav: 2
                    },
                    title: 'Analyze'
                }
            });
        }])
}());
