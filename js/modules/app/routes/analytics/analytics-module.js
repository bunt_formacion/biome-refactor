(function () {
    'use strict';

    angular
        .module('app.routes.analytics', ['app.core', 'vertex'])
        .value('dashboards', [])
        .config(['$urlRouterProvider', function ($urlRouterProvider) {
            $urlRouterProvider.when('/', '/analytics');
        }])
        .run(['CdeHelper', function (CdeHelper, dashboards) {
            dashboards = {
                charts: {
                    path: CdeHelper.getDashboardPath('/system/BioMe/dashboards/charts.wcdf'),
                    showProgress: true
                },
                sdr: {
                    path: CdeHelper.getDashboardPath('/system/BioMe/dashboards/datasetRequest.wcdf'),
                    showProgress: true
                }
            };
        }]);
})();
