(function () {
    'use strict';

    function HeartRateTimeController($scope, $stateParams, generateAnalyzerView, analyzerData) {
        var vm = this,
            stateName = 'analytics.analyze.heart-rate-time';

        vm.analyzerConfig = {
            cube: $stateParams.request,
            catalog: $stateParams.request,
            embeddingContext: 'biome,heart-rate-time',
            removeUndoButton: true,
            removeRedoButton: true,
            showFieldLayout: false,
            showFieldList: false,
            showFilterPanel: false
        };

        vm.onLoad = generateAnalyzerView($scope, getViewConfig, function () {
            return analyzerData.filterValue;
        });

        function getViewConfig(filterValue) {
            return {
                layout: {
                    rows: ["[Physical Stat].[Age Group]"],
                    //columns: [ "[Date.Year].[Year]" ],
                    measures: ["[Measures].[Avg Heart Rate BPM]"]
                },
                filters: {
                    "[Date.Year].[Year]": [{
                        "operator": "CONTAIN",
                        "members": [filterValue + ""]
                    }]
                },
                fieldOptions: {
                    // "[Measures].[Avg Heart Rate BPM]": {
                    //   formatExpression: "# bpm"
                    // },
                    "[Physical Stat].[Age Group]": {
                        "sortOrderEnum": "DESC"
                    }
                },
                vizId: "ccc_bar",
                title: "Avg. Heart Rate by Age Group",
                chartOptions: {
                    labelStyle: "BOLD"
                }
            };
        }

    }

    function HeartRateTimeSelectorController(analyzerData) {
        var vm = this,
            stateName = 'analytics.analyze.heart-rate-time';

        vm.data = analyzerData;

    }

    angular
        .module('app.routes.analytics')
        .run(['routerHelper', function (routerHelper) {
            routerHelper.configureStates({
                state: 'analytics.analyze.heart-rate-time',
                config: {
                    parent: 'analytics.analyze',
                    url: '/heart-rate-time',
                    deepStateRedirect: true,
                    sticky: true,
                    resolve: {
                        analyzerData: function () {
                            return {
                                filterValue: 2010
                            };
                        }
                    },
                    views: {
                        'analytics.analyze.heart-rate-time': {
                            templateUrl: 'partials/analytics/analyze/heart-rate-time/heart-rate-time-content.html',
                            controller: ['$scope', '$stateParams', 'generateAnalyzerView', 'analyzerData', HeartRateTimeController],
                            controllerAs: 'vm'
                        },
                        'analysis-title': {
                            template: 'Avg. Heart Rate Time Analysis'
                        },
                        'analysis-selector': {
                            templateUrl: 'partials/analytics/analyze/heart-rate-time/heart-rate-time-selector.html',
                            controller: ['analyzerData', HeartRateTimeSelectorController],
                            controllerAs: 'vm'
                        }
                    },
                    settings: {
                        nav: 2,
                        content: '<div class="toggle-button-icon bar-icon"></div>'
                    },
                    title: 'Avg. Heart Rate Time Analysis'
                }
            });
        }])
}());
