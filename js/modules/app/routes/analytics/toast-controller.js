(function () {
    'use strict';

    function ToastController($scope, $mdToast, action, actionData) {
        $scope.runAndHide = runAndHide;
        $scope.hide = hide;
        $scope.name = actionData.catalog || actionData.cube;

        function hide() {
            $mdToast.hide();
        }

        function runAndHide() {
            // The action order is reversed from the function name but... come on... it HAD to be this way!
            hide();
            action();
        }
    }

    angular
        .module('app.routes.analytics')
        .controller('ToastController', ['$scope', '$mdToast', 'action', 'actionData', ToastController]);
}());
