(function () {
    'use strict';

    function HeartRateController($scope, $stateParams, generateAnalyzerView, analyzerData) {
        var vm = this,
            stateName = 'analytics.analyze.heart-rate';

        vm.analyzerConfig = {
            cube: $stateParams.request,
            catalog: $stateParams.request,
            embeddingContext: 'biome,heart-rate',
            removeUndoButton: true,
            removeRedoButton: true,
            showFieldLayout: false,
            showFieldList: false,
            showFilterPanel: false
        };

        vm.onLoad = generateAnalyzerView($scope, getViewConfig, function () {
            return analyzerData.filterValue;
        });

        function getViewConfig(filterValue) {
            return {
                layout: {
                    rows: ["[Geography].[Country]"],
                    columns: ["[Health Stat].[Avg Calories Burned]"],
                    measures: ["[Measures].[Avg Heart Rate BPM]"]
                },
                vizId: "pivot"
            };
        }

    }

    function HeartRateSelectorController(analyzerData) {
        var vm = this,
            stateName = 'analytics.analyze.heart-rate';

        vm.data = analyzerData;

    }

    angular
        .module('app.routes.analytics')
        .run(['routerHelper', function (routerHelper) {
            routerHelper.configureStates({
                state: 'analytics.analyze.heart-rate',
                config: {
                    parent: 'analytics.analyze',
                    url: '/heart-rate',
                    deepStateRedirect: true,
                    sticky: true,
                    resolve: {
                        analyzerData: function () {
                            return {
                                filterValue: 87
                            };
                        }
                    },
                    views: {
                        'analytics.analyze.heart-rate': {
                            templateUrl: 'partials/analytics/analyze/heart-rate/heart-rate-content.html',
                            controller: ['$scope', '$stateParams', 'generateAnalyzerView', 'analyzerData', HeartRateController],
                            controllerAs: 'vm'
                        },
                        'analysis-title': {
                            template: 'Avg. Heart Rate'
                        },
                        'analysis-selector': {
                            templateUrl: 'partials/analytics/analyze/heart-rate/heart-rate-selector.html',
                            controller: ['analyzerData', HeartRateSelectorController],
                            controllerAs: 'vm'
                        }
                    },
                    settings: {
                        nav: 1,
                        content: '<div class="toggle-button-icon table-icon"></div>'
                    },
                    title: 'Avg. Heart Rate'
                }
            });
        }])
}());
