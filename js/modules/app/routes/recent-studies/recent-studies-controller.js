(function () {
    'use strict';

    function RecentStudiesController($q, logger) {
        var vm = this;

        vm.title = 'Locations';

    }

    angular
        .module('app.routes.locations')
        .run(['routerHelper', function (routerHelper) {
            routerHelper.configureStates({
                state: 'recentStudies',
                config: {
                    parent: 'root.details',
                    url: '/studies',
                    views: {
                        "recentStudies": {
                            templateUrl: 'partials/recent-studies/recent-studies.html',
                            controller: ['$q', 'logger', RecentStudiesController],
                            controllerAs: 'vm',
                        }
                    },
                    sticky: true,
                    deepStateRedirect: true,
                    title: 'Recent Studies',
                    settings: {
                        nav: 4,
                        content: '<i class="recent-studies-icon"></i> <span>Recent Studies</span>'
                    }
                }
            });
        }]);
}());
